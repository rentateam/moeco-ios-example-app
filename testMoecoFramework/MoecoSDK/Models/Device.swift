//
//  Device.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import CoreBluetooth

public struct Device {
    var name: String?
    var description: String
    var fromWhiteList: Bool
    
    var peripheral: CBPeripheral?
    var UUID: String
    var RSSI: NSNumber
    var connectable = "No"
    
    var advertisementDictionary: [String: Any]
    
    var services: [String: Service]
    
    init(peripheral: CBPeripheral, RSSI: NSNumber, advertisementDictionary: [String : Any], fromWhiteList: Bool, UUID: String, services: [String: Service]) {
        self.fromWhiteList = fromWhiteList
        self.peripheral = peripheral
        name = peripheral.name ?? "No name"
        self.UUID = UUID
        description = peripheral.description
        self.RSSI = RSSI
        if let isConnectable = advertisementDictionary[CBAdvertisementDataIsConnectable] as? NSNumber {
            connectable = (isConnectable.boolValue) ? "Yes" : "No"
        }
        self.services = services
        self.advertisementDictionary = advertisementDictionary
    }
    //For fake
    init(name: String?, description: String, fromWhiteList: Bool) {
        self.name = name
        self.description = description
        self.fromWhiteList = fromWhiteList
        peripheral = nil
        UUID = ""
        RSSI = NSNumber()
        connectable = "No"
        services = [String: Service]()
        advertisementDictionary = [String: Any]()
    }
    
    init(){
        name = nil
        description = ""
        fromWhiteList = false
        
        peripheral = nil
        UUID = ""
        RSSI = NSNumber()
        connectable = "No"
        services = [String: Service]()
        advertisementDictionary = [String: Any]()
    }
    
}
