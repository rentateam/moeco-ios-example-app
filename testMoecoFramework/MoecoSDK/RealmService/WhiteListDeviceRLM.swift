//
//  WhiteListDeviceRLM.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

class WhiteListDeviceRLM: Object {
    @objc dynamic var id = ""
    @objc dynamic var type = ""
    @objc dynamic var deviceHash = ""
    @objc dynamic var manufacturer = ""
    
    override class func primaryKey() -> String {
        return "deviceHash"
    }
}
