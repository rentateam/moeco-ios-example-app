//
//  WhiteListUpdateTimeRLM.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

//To keep in memory time of last update
class WhiteListUpdateTimeRLM: Object {
    @objc dynamic var id = ""
    @objc dynamic var time = Date()
    
    override class func primaryKey() -> String {
        return "id"
    }
}
