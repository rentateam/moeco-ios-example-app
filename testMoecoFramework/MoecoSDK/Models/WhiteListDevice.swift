//
//  WhiteListDevice.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation

public struct WhiteListDevice {
    let id: String
    let type: String
    let hash: String
    let manufacturer: String
}
