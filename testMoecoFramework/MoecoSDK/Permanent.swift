//
//  Permanent.swift
//  moeco
//
//  Created by Алексей Россошанский on 28.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreLocation

struct Permanent {
    
    class BLE {
        static var arrayOfServicesToSearch = [CBUUID(string: "0000A000-6824-11E5-8266-0002A5D5C51B"), CBUUID(string: "FEAA"), CBUUID(string: "FE59"), CBUUID(string: "EF680100-9B35-4933-9B10-52FFA9740042")]
    }
    
    class IBeacon {
        static var proximityUUIDToMonitor = UUID(uuidString: "00000000-6824-11E5-8266-0002A5D5C51B")!
        static var ibeaconRegion = CLBeaconRegion(proximityUUID: Permanent.IBeacon.proximityUUIDToMonitor, major: 24465, minor: 42381, identifier: "ClickBeacon")
        //let beaconRegion2 = CLBeaconRegion(proximityUUID: uuid, major: 25660, minor: 32301, identifier: "PermanentBeacon")
        //let beaconRegion3 = CLBeaconRegion(proximityUUID: uuid, identifier: "GENERALuuidBEACON")
        
    }
    
    class Network {
        static var token = "dd89de561fb5fad9ba9a1674e5d2d93c"
        static var baseURL = "https://dev01.moeco.io/"
    }
    
}
