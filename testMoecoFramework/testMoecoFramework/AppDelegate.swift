//
//  AppDelegate.swift
//  testMoecoFramework
//
//  Created by Алексей Россошанский on 07.04.18.
//  Copyright © 2018 Alexey Rossoshasky. All rights reserved.
//

import UIKit
import MoecoSDK
import SwiftyJSON
import Alamofire
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    var moecoSDK = MoecoSDK()
    
    //bluetooth
    lazy var bluetoothService: BluetoothService = {
        return BluetoothService(moecoSDK: self.moecoSDK)
    }()
    //iBeacon
    var iBeaconManager = IBeaconManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        //Realm
        moecoSDK.configureDataBase()
        
        //Конфигурации из БД (если есть)
        moecoSDK.setConfigurations()

        //bluetooth
        //Передаем в bluetoothService moecoSDK , чтобы внутри bluetoothService иметь доступ к свойствам fakeSDK
        bluetoothService = BluetoothService(moecoSDK: moecoSDK)
        bluetoothService.initializeCentralManager(whoDelegate: bluetoothService)
        //iBeacon
        iBeaconManager.initializeLocationManager(whoDelegate: iBeaconManager, bluetoothService: bluetoothService)
        iBeaconManager.authorizeLocationServices()
//
        //Если зашли в первый раз (Hash empty), то авторизируемся и запускаем периодическое сканирование и периодическую отправку на сервак. Если не впервые, то отправляем на сервак собранные данные (если есть), если такая опция включена в настройках и запускаем периодическое сканирование, если такая опция включена в настройках
        moecoSDK.createHashOrFetchExisted()
        
        
        //Это позволяет таймеру достаточно долго работать в бэкграунде
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        bluetoothService.startPeriodicScan()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        //Конфигурации
        moecoSDK.setConfigurations()
        
        //Подгружаем whitelist
        //Если возможность загрузки whitelist-a включена, то подгружаемwhitelist
        if moecoSDK.getmasterDataExchenge() == true {
            NetworkService.fetchWhiteList(forcibly: false)
        }
        
        if moecoSDK.gettransactionsExchange() == true {
            NetworkService.stopPeriodicSendTimer()
            NetworkService.startPeriodicSendTimer()
        }
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

