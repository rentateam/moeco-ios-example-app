//
//  BluetoothService.swift
//  moeco
//
//  Created by Алексей Россошанский on 15.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import CoreBluetooth
import RealmSwift

public class BluetoothService: NSObject {
    
    
    var timerForFinish = Timer()
    var timerForStart = Timer()
    
    var centralManager: CBCentralManager!
    private var discoveredDevice = Device()
    private var dictionaryOfDiscoveredDevice = [String: Device]()
    //Переменная для проверки на заполенние всех характеристик
    private var finishedCharacteristicReadingAmount = 0
    
    var moecoSDK: MoecoSDK
    
    public init(moecoSDK: MoecoSDK) {
        self.moecoSDK = moecoSDK
    }
    
    
    //MARK: - Initialization
    public func initializeCentralManager(whoDelegate: CBCentralManagerDelegate) {
        centralManager = CBCentralManager(delegate: whoDelegate, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }
    
}




//MARK: - CentralManager delegates methods
extension BluetoothService: CBCentralManagerDelegate {
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOff:
            Helpers.LogCreater.writeLog(string: "State : Powered Off")
        case .poweredOn:
            Helpers.LogCreater.writeLog(string: "CENTRAL MANAGER STATE CHANGED")
            if UIApplication.shared.applicationState != .background {
                if moecoSDK.bleSystem == true {
                    startPeriodicScan()
                }
            }
            Helpers.LogCreater.writeLog(string: "Powered on")
        case .resetting:
            Helpers.LogCreater.writeLog(string: "Resetting")
        case .unauthorized:
            Helpers.LogCreater.writeLog(string: "Unauthorized")
        case .unknown:
            Helpers.LogCreater.writeLog(string: "Unknown")
        case .unsupported:
            Helpers.LogCreater.writeLog(string: "Unsupported")
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        Helpers.LogCreater.writeLog(string: "PERIPHERAL DISCOVERED: \(peripheral.identifier)")
        discoveredDevice = Device(peripheral: peripheral, RSSI: RSSI, advertisementDictionary: advertisementData, fromWhiteList: true, UUID: peripheral.identifier.uuidString , services: [String: Service]())
        RealmService.shared.addDeviceIntoRLM(device: discoveredDevice)
        //Если вернуть эту строку то в базу данных будут добавляться найденные ibeaconы
        //        RealmService.shared.addTransactionIntoRLM(scannedDevice: discoveredDevice)
        dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString] = discoveredDevice
    }
    
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        Helpers.LogCreater.writeLog(string: "PERIPHERAL \(peripheral.identifier)) CONNECTED")
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        Helpers.LogCreater.writeLog(string: "START DISCOVERING SERVICES")
        
    }
    
}

//MARK: - Peripheral delegates methods
extension BluetoothService: CBPeripheralDelegate {
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        Helpers.LogCreater.writeLog(string: "SERVICES FOUNDED")
        guard let services = peripheral.services else {centralManager.cancelPeripheralConnection(peripheral); return}
        
        
        services.forEach { (service) in
            
            var newService = Service()
            newService.service = service
            newService.uuidOfService = service.uuid.uuidString
            
            dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services[service.uuid.uuidString] = newService
            Helpers.LogCreater.writeLog(string: "START DISCOVERING CHARACTERISTIC FOR SERVICE: \(newService.uuidOfService)")
            peripheral.discoverCharacteristics(nil, for: service)
        }
        
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        Helpers.LogCreater.writeLog(string: "CHARACTERISTICS FOR SERVICE \(service.uuid.uuidString) FOUNDED")
        guard let characteristics = service.characteristics else { print("There is no characteristics in service \(service.uuid.uuidString)"); return}
        
        
        characteristics.forEach { (characteristic) in
            
            //Если характеристика readable и not notify
            if characteristic.properties.contains(.read) && !characteristic.properties.contains(.notify) {
                
                var newCharacteristic = Characteristic()
                newCharacteristic.characteristic = characteristic
                newCharacteristic.uuid = characteristic.uuid.uuidString
                dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services[service.uuid.uuidString]?.characteristics[characteristic.uuid.uuidString] = newCharacteristic
                
                //Считываем значение
                peripheral.readValue(for: characteristic)
                Helpers.LogCreater.writeLog(string: "START READING CHARACTERISTIC VALUE FOR CHARACTERISTIC: \(characteristic.uuid.uuidString)")
            }
        }
    }
    
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        Helpers.LogCreater.writeLog(string: "HAVE READ CHARACTERISTIC VALUE FOR CHARACTERISTIC: \(characteristic.uuid.uuidString)")
        guard let data = characteristic.value else { return }
        let valueDataEncodedIntoString = data.hexEncodedString(options: .upperCase)
        
        finishedCharacteristicReadingAmount += 1
        dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services[characteristic.service.uuid.uuidString]?.characteristics[characteristic.uuid.uuidString]?.value = valueDataEncodedIntoString
        
        //Ставим метку, что значение прочитано (чтобы не делать проверку на "пустоту") и обезопаситься от пустых значений
        dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services[characteristic.service.uuid.uuidString]?.characteristics[characteristic.uuid.uuidString]?.done = true
        
        //Добавляем в базу данных только тогда, когда все характеристики прочитаны
        //Считаем, сколько всего характеристик у данного девайса
        var amountOfCharacteristicFactual: Int {
            var amountOfCharacteristicCollector = 0
            dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services.forEach({ (key, value) in
                amountOfCharacteristicCollector += (dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]?.services[key]?.characteristics.count)!
            })
            return amountOfCharacteristicCollector
        }
        
        //Если кол-во прочитанных характеристик = общему числу характеристик у девайса, то добавляем в БД и отсоединяемся (+ обнуляем накапливающую переменную)
        if finishedCharacteristicReadingAmount == amountOfCharacteristicFactual {
            RealmService.shared.addTransactionIntoRLM(scannedDevice: dictionaryOfDiscoveredDevice[peripheral.identifier.uuidString]!)
            centralManager.cancelPeripheralConnection(peripheral)
            finishedCharacteristicReadingAmount = 0
        }
        
    }
    
    
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        Helpers.LogCreater.writeLog(string: "PERIPHERAL DISCONNECTED")
    }
    
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        Helpers.LogCreater.writeLog(string: "CONNECTION TO \(peripheral.identifier.uuidString) WAS FAILED BECAUSE OF \(String(describing: error?.localizedDescription))")

    }
    
}


//MARK: - Periodic scanning
extension BluetoothService {
    
    //Connect
    public func connectToPeripherals(peripherals: [String: Device]) {
        
        peripherals.forEach { (key, value) in
            if peripherals[key]?.connectable == "Yes" {
                guard let peripheral = peripherals[key]?.peripheral else {return}
                centralManager.connect(peripheral, options: nil)
                Helpers.LogCreater.writeLog(string: "START CONNECTING....")
            } else {
                //Здесь реализация для IBeacon - ов
                return
            }
        }
    }
    
    @objc func startPeriodicScan() {
        
        if centralManager.isScanning {
            centralManager.stopScan()
        }
        
        //Если включена возможность сканирования BLE, то сканируем, иначе выходим из функции
        if moecoSDK.bleSystem == false { return }
        Helpers.LogCreater.writeLog(string: "PERIODIC SCAN STARTED")
        dictionaryOfDiscoveredDevice.removeAll()
        RealmService.shared.deleteDevicesFromRLM()
        
        timerForStart.invalidate()
        timerForStart = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(stopPeriodicScan), userInfo: nil, repeats: false)
        
        centralManager.scanForPeripherals(withServices: Permanent.BLE.arrayOfServicesToSearch, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        
    }
    
    @objc func stopPeriodicScan(){
        Helpers.LogCreater.writeLog(string: "PERIODIC SCAN STOPPED")
        centralManager.stopScan()
        
        if moecoSDK.bleSystem == false { return }

        connectToPeripherals(peripherals: dictionaryOfDiscoveredDevice)
        
        timerForFinish.invalidate()
        timerForFinish = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(startPeriodicScan), userInfo: nil, repeats: false)
    }
    
}




