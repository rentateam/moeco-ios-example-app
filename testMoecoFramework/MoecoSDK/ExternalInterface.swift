//
//  ExternalInterface.swift
//  moeco
//
//  Created by Алексей Россошанский on 12.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation

public protocol MoecoDelegate {
    
    func fetchData(closure: @escaping ([String]) -> Void)
    
    func getDevices() -> [Device]
    
    func getBadTransactions(closure: @escaping ([Transaction]) -> Void)
    
    func getInvoices(closure: @escaping ([Invoice]) -> Void)
    
    func getPayments(closure: @escaping ([Payment]) -> Void )
    
    func takeHash() -> String
    
    func getmoecoService() -> Bool
    func moecoServiceSwitch()
    
    func gettransactionsExchange() -> Bool
    func transactionsExchangeSwitch()
    
    func getmasterDataExchenge() -> Bool
    func masterDataExchengeSwitch()
    
    func getBLESystem() -> Bool
    func bleSystemSwitch()
    
    func generateFakeTransactions()
    
    func setConfigurations()
    
    func createHashOrFetchExisted()
    
    func configureDataBase()

}
