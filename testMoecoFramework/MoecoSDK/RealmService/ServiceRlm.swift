//
//  ServiceRlm.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

class ServiceRlm: Object {
    
    @objc dynamic var uuid = ""
    var characteristics = List<CharacteristicRlm>()
    
    private enum CodingKeys: String, CodingKey {
        case uuid, characteristics
    }
    
    override class func primaryKey() -> String {
        return "uuid"
    }
    
}
