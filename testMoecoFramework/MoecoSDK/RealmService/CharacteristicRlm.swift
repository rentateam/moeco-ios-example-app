//
//  CharacteristicRlm.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

class CharacteristicRlm: Object {
    
    @objc dynamic var uuid = ""
    @objc dynamic var value = ""
    
    override class func primaryKey() -> String {
        return "uuid"
    }
}
