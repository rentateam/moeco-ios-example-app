//
//  Transaction.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation

public struct Transaction {
    let id: String
    let status: String
    let cause: String
    let statusDescribing: String
    
    init(id: String, status: String, cause: String) {
        self.id = id
        self.status = status
        self.cause = cause
        
        switch status {
        case "1":
            statusDescribing = "Init"
        case "2":
            statusDescribing = "Declined"
        case "3":
            statusDescribing = "Accepted"
        case "4":
            statusDescribing = "Refused"
        case "5":
            statusDescribing = "Valid"
        case "6":
            statusDescribing = "Invoiced"
        case "7":
            statusDescribing = "Invoice Approved"
        case "8":
            statusDescribing = "Payment Init"
        case "9":
            statusDescribing = "Paid"
        case "10":
            statusDescribing = "Closed"
        default:
            statusDescribing = "Unknown"
        }
    }
    
}
