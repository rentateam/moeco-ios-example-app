//
//  Payment.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation

public struct Payment {
    let status: String
    let sum: String
    let time: String
    let statusDescribing: String
    
    init(status: String, sum: String, time: String) {
        self.status = status
        self.sum = sum
        self.time = time
        
        switch status {
        case "1":
            statusDescribing = "Init"
        case "2":
            statusDescribing = "Paid"
        case "3":
            statusDescribing = "Closed"
        default:
            statusDescribing = "Unknown"
        }
    }
    
}
