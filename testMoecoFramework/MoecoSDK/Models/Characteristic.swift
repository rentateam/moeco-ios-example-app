//
//  Characteristic.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import CoreBluetooth

public struct Characteristic {
    var value: String
    var uuid: String
    var done = false
    var characteristic: CBCharacteristic?
    
    init() {
        value = ""
        uuid = ""
        characteristic = nil
    }
}
