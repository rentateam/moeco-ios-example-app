//
//  LocationManager.swift
//  moeco
//
//  Created by Алексей Россошанский on 16.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth



public class IBeaconManager: NSObject {
    
    var locationManager: CLLocationManager!
    var bluetoothService: BluetoothService!
    
    
    //Initialization
   public func initializeLocationManager(whoDelegate: CLLocationManagerDelegate, bluetoothService: BluetoothService) {
        locationManager = CLLocationManager()
        locationManager.delegate = whoDelegate
        self.bluetoothService = bluetoothService
    }
    
    
    //Authorization
    public func authorizeLocationServices() {
        let status = CLLocationManager.authorizationStatus()
        print("Current Location Services Authorization Status: \(status.rawValue)")
        if (status != CLAuthorizationStatus.authorizedAlways) {
            if locationManager.responds(to: #selector(CLLocationManager.requestAlwaysAuthorization)){
                locationManager.requestAlwaysAuthorization()
            }
        } else {  
            
        }
    }
    
    //Monitoring
    public func startMonitoring() {
        locationManager.startMonitoring(for: Permanent.IBeacon.ibeaconRegion)
    }
    
    public func stopMonitoring() {
        locationManager.stopMonitoring(for: Permanent.IBeacon.ibeaconRegion)
    }
    
}

//MARK: - CoreLocation delegates methods (iBeacon)
extension IBeaconManager: CLLocationManagerDelegate{
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedAlways {
            if bluetoothService.moecoSDK.bleSystem {
            startMonitoring()
            }
        }
        
    }
    
    //Присутствует задержка вызова метода в фоновом режиме (~30сек)
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        Helpers.LogCreater.writeLog(string: "EXIT REGION \(region.identifier)")

//        if bluetoothService.centralManager.isScanning {
//            bluetoothService.stopPeriodicScan()
//        } else {
//            bluetoothService.startPeriodicScan()
//        }
//        NetworkService.stopPeriodicSendTimer()
//        NetworkService.startPeriodicSendTimer()
//        
//        //И подгружаем whiteList
//        if bluetoothService.moecoSDK.masterDataExchenge == true {
//            NetworkService.fetchWhiteList(forcibly: false)
//        }
    }
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        Helpers.LogCreater.writeLog(string: "ENTER REGION \(region.identifier)")
        
        bluetoothService.timerForStart.invalidate()
        bluetoothService.timerForFinish.invalidate()
        bluetoothService.startPeriodicScan()
        
        
        NetworkService.stopPeriodicSendTimer()
        NetworkService.startPeriodicSendTimer()
        
        //И подгружаем whiteList
        if bluetoothService.moecoSDK.masterDataExchenge == true {
            NetworkService.fetchWhiteList(forcibly: false)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        //state.rawValue = 1 - в регионе , = 2 - не в регионе
        Helpers.LogCreater.writeLog(string: "DETERMINE STATE: \(state.rawValue)")
    }
    
}





