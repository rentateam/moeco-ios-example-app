//
//  TransactionRlm.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

class TransactionRlm: Object {
    
    @objc dynamic var peripheralIdentifier = ""
    @objc dynamic var connectableStatus = ""
    @objc dynamic var fromWhiteList = Bool()
    var services = List<ServiceRlm>()
    
    override class func primaryKey() -> String {
        return "peripheralIdentifier"
    }
    
}
