//
//  Helpers.swift
//  moeco
//
//  Created by Алексей Россошанский on 22.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

struct Helpers {}

extension Helpers {
    class UUIDCreater {
        static func createUUID() -> String {
            let uuid = UUID().uuidString
            return uuid
        }
    }
    
    
    class ParametersForRequestCrater {
        static func createParameters(transactions: Results<TransactionRlm>) -> [String: Any]{
            
            var allTransactionsForparameters = [[String: Any]]()
            
            var servicesDictionary = [String: Any]()
            var characteristicDictionary = [String: Any]()
            transactions.forEach { (transaction) in
                transaction.services.forEach({ (service) in
                    service.characteristics.forEach({ (characteristic) in
                        characteristicDictionary[characteristic.uuid] = characteristic.value
                    })
                    servicesDictionary[service.uuid] = characteristicDictionary
                    characteristicDictionary.removeAll()
                })
                
                let oneTransactionInParameter: [String: Any] = ["hash": "\(RealmService.shared.fetchUserHashFromRealm())",
                                                                "device_hash": "ec:15:9e:ff:87:8a",
                                                                "status": 1,
                                                                "timestamp": Date().description,
                                                                "uplink": false,
                                                                "payload": String(data: JSONSerializer.dictToJson(dictionary: servicesDictionary), encoding: .utf8)!
                                                                ]
                
                allTransactionsForparameters.append(oneTransactionInParameter)
                
                servicesDictionary.removeAll()
            }
            
            
            let parameters: [String: Any] = ["transactions": allTransactionsForparameters,
                                             "uplink": ""]
            
            
            return parameters
        }
    }
    
    
    class JSONSerializer {
        static func dictToJson(dictionary:[String: Any]) -> Data {
            var jsonData = Data()
            do {
                jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
            return jsonData
        }
    }
    
    class LogCreater {
        static func writeLog(string: String) {
            //Пишем логи в зависимости от сборки
            let config = Bundle.main.infoDictionary?["Configuration"] as! String
            if config == "Debug" {
                NSLog("%@", string)
            }

        }
    }
    
}



