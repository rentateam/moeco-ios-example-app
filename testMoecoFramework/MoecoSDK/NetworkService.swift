//
//  NetworkService.swift
//  moeco
//
//  Created by Алексей Россошанский on 22.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift


//MARK: - Get and post data to server
public class NetworkService {
    
    static weak var timerForPeriodicSendData: Timer?
    

    static func authorization(apiKey: String){
        let url = URL(string: Permanent.Network.baseURL + "api/gate/auth/")!
        let hash = Helpers.UUIDCreater.createUUID()
        let parameters: [String: Any] = ["api_key": apiKey,
                                         "gate": ["hash" : hash]]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //Body of request
//                print (NSString(data:response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!)
                let fetchedHash = (json["data"].arrayValue)[0]["hash"].stringValue
                
                RealmService.shared.addUserIntoRLM(hash: fetchedHash)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func sendTransactionToServer() {
   
        guard let existed = RealmService.shared.realm.objects(TransactionRlm.self).first else {Helpers.LogCreater.writeLog(string: "NOTHING TO SEND"); return}
        let transactions = RealmService.shared.realm.objects(TransactionRlm.self)
        
        Helpers.LogCreater.writeLog(string: "DATA SEND TO SERVER")
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/sync?last_sync=2018-01-11T14:13:15Z")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        
        let parameters = Helpers.ParametersForRequestCrater.createParameters(transactions: transactions)

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let _):
                 Helpers.LogCreater.writeLog(string: "PERIODIC DATA EXCHENGE SUCCEED")

                //Удаляем транзакции и все, что с ними связано из БД
                RealmService.shared.deleteAllTransactionConnected()
                 
                Helpers.LogCreater.writeLog(string: "DATABASE CLEARED")

                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    static func fetchInvoices(success: @escaping (_ resp: [Invoice]) -> Void) {
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/invoices")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        

        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let jsonArray = json["data"].arrayValue
                
                var invoicesArray = [Invoice]()
                
                jsonArray.forEach({ (item) in
                    
                    let status = item["status"].stringValue
                    let sum = item["amount"].stringValue
                    let updated_at = item["updated_at"].stringValue
                    let newInvoice = Invoice.init(status: status, sum: sum, time: updated_at)
                    
                    invoicesArray.append(newInvoice)
                })
                
                success(invoicesArray)
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    
    static func fetchPayments(success: @escaping (_ resp: [Payment]) -> Void) {
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/payments")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let jsonArray = json["data"].arrayValue
                
                var paymentsArray = [Payment]()
                
                jsonArray.forEach({ (item) in
                    
                    let status = item["status"].stringValue
                    let sum = item["amount"].stringValue
                    let paid_add = item["paid_add"].stringValue
                    let newPayment = Payment.init(status: status, sum: sum, time: paid_add)
                    
                    paymentsArray.append(newPayment)
                })
                
                success(paymentsArray)
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    static func fetchBadTransactions(success: @escaping (_ resp: [Transaction]) -> Void) {
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/bad_transactions")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let jsonArray = json["data"].arrayValue
                
                var transactionsArray = [Transaction]()
                
                jsonArray.forEach({ (item) in
                    
                    let status = item["status"].stringValue
                    let hash = item["hash"].stringValue
                    let rejection_reason = item["rejection_reason"].stringValue
                    let newTransaction = Transaction.init(id: hash, status: status, cause: rejection_reason)

                    transactionsArray.append(newTransaction)
                })
                
                success(transactionsArray)
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    static func fetchWhiteList(forcibly: Bool) {
        //Если принудительно, то не проверяем, прошел ли час
        if !forcibly {
        //Если еще часа не прошло, то подгружаем
        if RealmService.shared.isTimeOfWhiteListLastUpdatingEarlierThanHour() { return }
        }
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/devices")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        
        //Зачищаем WhiteList в Realm
        RealmService.shared.deleteWhiteListFromRLM()
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                
                Helpers.LogCreater.writeLog(string: "WHITE LIST FETCHED")
                
                
                let json = JSON(value)
                let jsonArray = json["data"].arrayValue
                
                
                jsonArray.forEach({ (item) in
 
                    let id = item["id"].stringValue
                    let type = item["type"].stringValue
                    let hash = item["hash"].stringValue
                    let manufacturer = item["manufacturer"].stringValue
                    
                    let newWhiteListDevice = WhiteListDevice.init(id: id, type: type, hash: hash, manufacturer: manufacturer)
                    //Добавляем его в базу
                    RealmService.shared.addWhiteListDeviceIntoRealm(whiteListDevice: newWhiteListDevice)
                    
                })
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    static func fetchAgregatedData(success: @escaping (_ response: [String]) -> Void) {
        
        let url = URL(string: Permanent.Network.baseURL + "api/gate/v2/infos")!
        let headers = ["Authorization": "Gateway: \(RealmService.shared.fetchUserHashFromRealm())"]
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let jsonArray = json["data"].arrayValue
                
                var arrayOfStatusesCounts = [String]()
                
                jsonArray.forEach({ (item) in
                    let count = item["count"].stringValue
                    arrayOfStatusesCounts.append(count)
                })
                success(arrayOfStatusesCounts)
                
            case .failure(let error):
                print(error)
            }
        }
        
    }

}


//MARK: - Periodic send data
extension NetworkService {
    
    static func startPeriodicSendTimer() {
        
        
        //Отправляем сразу после вызова функции
        NetworkService.sendTransactionToServer()
        
        //Затем каждые 30 секунд
        timerForPeriodicSendData?.invalidate()
        timerForPeriodicSendData = Timer.scheduledTimer(withTimeInterval: 180, repeats: true) { _ in
            
            NetworkService.sendTransactionToServer()
        
        }
    }
    
    static func stopPeriodicSendTimer() {
        timerForPeriodicSendData?.invalidate()
    }
    
}

