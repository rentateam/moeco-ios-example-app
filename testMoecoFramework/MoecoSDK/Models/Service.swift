//
//  Service.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import CoreBluetooth

public struct Service {
    var service: CBService?
    var characteristics: [String: Characteristic]
    var uuidOfService: String
    init() {
        service = nil
        characteristics = [String: Characteristic]()
        uuidOfService = ""
    }
}
