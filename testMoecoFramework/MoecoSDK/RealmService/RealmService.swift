//
//  RealmService.swift
//  moeco
//
//  Created by Алексей Россошанский on 19.03.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

class RealmService {
    
    static let shared = RealmService()
    
    var realm: Realm!
    
    func createObjectInRealm<T: Object>(_ object: T) {
        do{ try realm.write {
            realm.add(object, update: true)
            }
        } catch {
            print("Creating object error")
        }
    }
    
    func createHash<T: Object>(_ object: T) {
        do{ try realm.write {
            realm.add(object)
            }
        } catch {
            print("Creating object error")
        }
    }
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print("Object deleting error")
        }
    }
    
    
    func deleteALL(){
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func createRealm() {
        realm = try! Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    }
    
    
    func addUserIntoRLM(hash: String) {
        let user = UserRLM()
        user.userHash = hash
        
        createHash(user)
    }
    
    func fetchUserHashFromRealm() -> String {
        let userList = RealmService.shared.realm.objects(UserRLM.self)
        if let user = userList.first {
           return user.userHash
        } else {
            return "Undefined user"
        }
    }
    
    func addTransactionIntoRLM(scannedDevice: Device){
        
        var serviceRLM = ServiceRlm()
        let transactionRLM = TransactionRlm()
        
        scannedDevice.services.forEach { (key, value) in
            
            guard let service = scannedDevice.services[key] else {return}
            scannedDevice.services[key]?.characteristics.forEach({ (key2, value2) in
                
                guard let characteristic = scannedDevice.services[key]?.characteristics[key2] else { return }
                let characteristicRLM = CharacteristicRlm()
                characteristicRLM.uuid = characteristic.uuid
                characteristicRLM.value = characteristic.value
                serviceRLM.characteristics.append(characteristicRLM)
                
            })
            
            serviceRLM.uuid = service.uuidOfService
            transactionRLM.services.append(serviceRLM)
            serviceRLM = ServiceRlm()
            
        }
        
        transactionRLM.connectableStatus = "1"
        transactionRLM.fromWhiteList = true
        transactionRLM.peripheralIdentifier = scannedDevice.UUID
        
        Helpers.LogCreater.writeLog(string: "TRANSACTION ADDED TO DATABASE")
        createObjectInRealm(transactionRLM)

    }
    
    func addDeviceIntoRLM(device: Device) {
        let deviceRLM = DeviceRLM()
        if let name = device.name {
            deviceRLM.name = name
        }
        deviceRLM.uuid = device.UUID
        deviceRLM.fromWhiteList = device.fromWhiteList
        createObjectInRealm(deviceRLM)
    }
    
    func addWhiteListDeviceIntoRealm(whiteListDevice: WhiteListDevice) {
        
        //Добавляем время последнего обновления white-list-a (чтобы обновлять раз в час)
        let whiteListUpdateTimeRLM = WhiteListUpdateTimeRLM()
        whiteListUpdateTimeRLM.id = "main"
        whiteListUpdateTimeRLM.time = Date()
        
        createObjectInRealm(whiteListUpdateTimeRLM)
        
        
        let whiteListDeviceRLM = WhiteListDeviceRLM()
        
        whiteListDeviceRLM.id = whiteListDevice.id
        whiteListDeviceRLM.deviceHash = whiteListDevice.hash
        whiteListDeviceRLM.type = whiteListDevice.type
        whiteListDeviceRLM.manufacturer = whiteListDevice.manufacturer
        
        createObjectInRealm(whiteListDeviceRLM)
    }
    
    func deleteWhiteListFromRLM(){
        guard let existed = RealmService.shared.realm.objects(WhiteListDeviceRLM.self).first else {return}
        let whiteList = RealmService.shared.realm.objects(WhiteListDeviceRLM.self)
        do {
            try realm.write {
                realm.delete(whiteList)
                
            }
        } catch {
            print("Objects deleting error")
        }
    }
    
    //Здесь true, если white лист еще не существует или же если прошло меньше чем 60 минут
    func isTimeOfWhiteListLastUpdatingEarlierThanHour() -> Bool {
        guard let existedDate = RealmService.shared.realm.objects(WhiteListUpdateTimeRLM.self).first else {return false
        }
        
        let currentDate = Date()
        
        let interval = currentDate.timeIntervalSince(existedDate.time)
        
        if interval < 3600 {
            return true
        } else {
            return false
        }

    }
    
    
    func deleteDevicesFromRLM(){
        guard let existed = RealmService.shared.realm.objects(DeviceRLM.self).first else {return}
        let devicesList = RealmService.shared.realm.objects(DeviceRLM.self)
        do {
            try realm.write {
                realm.delete(devicesList)
                
            }
        } catch {
            print("Objects deleting error")
        }
        
    }
    
    
    func deleteTransactionsFromRLM(){
        guard let existed = RealmService.shared.realm.objects(TransactionRlm.self).first else {return}
        let transactionsList = RealmService.shared.realm.objects(TransactionRlm.self)
        do {
            try realm.write {
                realm.delete(transactionsList)
            }
        } catch {
            print("Objects deleting error")
        }
        
    }
    
    func deleteServicesFromRLM() {
        guard let existed = RealmService.shared.realm.objects(ServiceRlm.self).first else {return}
        let servicesList = RealmService.shared.realm.objects(ServiceRlm.self)
        do {
            try realm.write {
                realm.delete(servicesList)
            }
        } catch {
            print("Objects deleting error")
        }
    }
    
    func deleteCharacteristicsFromRLM() {
        guard let existed = RealmService.shared.realm.objects(CharacteristicRlm.self).first else {return}
        let characteristicsList = RealmService.shared.realm.objects(CharacteristicRlm.self)
        do {
            try realm.write {
                realm.delete(characteristicsList)
            }
        } catch {
            print("Objects deleting error")
        }
    }

    
    func deleteAllTransactionConnected(){
        
    deleteTransactionsFromRLM()
    deleteServicesFromRLM()
    deleteCharacteristicsFromRLM()
    }
    
    func isTransactionRLMEmpty() -> Bool {
//        var p = RealmService.shared.realm.objects(TransactionRlm.self).count
        guard let existed = RealmService.shared.realm.objects(TransactionRlm.self).first else {return true}
        return false
    }
    
    //Подгружаем сохраненные конфигурации или же создаем объект конфигураций при первом запуске приложения
    func downloadConfigurationFromRLM() -> ConfigurationsRLM {
        
        if let existedConfiguration = RealmService.shared.realm.objects(ConfigurationsRLM.self).first {
            return existedConfiguration
        } else {
            let configurationRLM = ConfigurationsRLM()
            configurationRLM.bleListener = true
            configurationRLM.transactionalDataExchange = true
            configurationRLM.masterDataExchange = true
            configurationRLM.moecoService = true
            
            createObjectInRealm(configurationRLM)
            return configurationRLM
        }
        
   
    }
    
}

