//
//  FakeSDK.swift
//  moeco
//
//  Created by Anton on 13/03/2018.
//  Copyright © 2018 rentateam. All rights reserved.
//

import UIKit
import CoreBluetooth

public class MoecoSDK: MoecoDelegate {
    
    var moecoService = true
    var transactionsExchange = true
    var masterDataExchenge = true
    var bleSystem = true
    
    public init(){}
    
    public func fetchData(closure: @escaping ([String]) -> Void){
        
        NetworkService.fetchAgregatedData { (responseData) in
            closure(responseData)
        }
    }
    
    public func getDevices() -> [Device]  {
        
        var devicesNear = [Device]()
        
        let dataFromDataBase = RealmService.shared.realm.objects(DeviceRLM.self)
        dataFromDataBase.forEach({ (device) in
            let device = Device.init(name: device.name, description: device.uuid, fromWhiteList: device.fromWhiteList)
            devicesNear.append(device)
        })
        
        
        return devicesNear
    }
    
    public func getBadTransactions(closure: @escaping ([Transaction]) -> Void ){
        
        var badTransactions = [Transaction]()
        
        NetworkService.fetchBadTransactions { (transactionsFetched) in
            badTransactions = transactionsFetched
            closure(badTransactions)
        }
        
    }
    
    public func getInvoices(closure: @escaping ([Invoice]) -> Void ){
        
        var invoices = [Invoice]()
        
        NetworkService.fetchInvoices { (invoicesFetched) in
            invoices = invoicesFetched
            closure(invoices)
        }
        
        
    }
    
    public func getPayments(closure: @escaping ([Payment]) -> Void ){
        
        var payments = [Payment]()

        
        NetworkService.fetchPayments { (paymentsFetched) in
            payments = paymentsFetched
            closure(payments)
        }

    }
    
    public func takeHash() -> String {
        let hash = RealmService.shared.fetchUserHashFromRealm()
        return hash
        
    }
    public func getmoecoService() -> Bool {
        return moecoService
    }
    
    public func gettransactionsExchange() -> Bool {
        return transactionsExchange
    }
    
    public func getmasterDataExchenge() -> Bool {
        return masterDataExchenge
    }
    
    public func getBLESystem() -> Bool {
        return  bleSystem
    }
    
    //В каждой из след функций выгружаем из базы данных имеющюся конфигурации, меняем нужное значение и сохраняем (
    public func moecoServiceSwitch() {
        Helpers.LogCreater.writeLog(string: "MOECO SERVICE SWITCHED")
        
        moecoService = !moecoService
        
        //а также выкл/вкл все конфигурации
        transactionsExchange = moecoService
        masterDataExchenge = moecoService
        bleSystem = moecoService
        
        let configuration = RealmService.shared.downloadConfigurationFromRLM()
     
        try! RealmService.shared.realm.write({
            configuration.moecoService = moecoService
            //а также выкл/вкл все конфигурации (в БД)
            configuration.transactionalDataExchange = transactionsExchange
            configuration.bleListener = bleSystem
            configuration.masterDataExchange = masterDataExchenge
            
            RealmService.shared.realm.add(configuration)
        })
        
    }
    
    public func transactionsExchangeSwitch() {
        Helpers.LogCreater.writeLog(string: "TRANSACTIONS DATA EXCHENGE SWITCHED")
        transactionsExchange = !transactionsExchange
        
        let configuration = RealmService.shared.downloadConfigurationFromRLM()
        
        try! RealmService.shared.realm.write({
            configuration.transactionalDataExchange = transactionsExchange
            RealmService.shared.realm.add(configuration)
        })
        
        if transactionsExchange {
            NetworkService.startPeriodicSendTimer()
        } else {
            NetworkService.stopPeriodicSendTimer()
        }
        
    }
    
    public func masterDataExchengeSwitch() {
        Helpers.LogCreater.writeLog(string: "MASTER DATA EXCHENGE SWITCHED")
        masterDataExchenge = !masterDataExchenge
        
        let configuration = RealmService.shared.downloadConfigurationFromRLM()
        
        try! RealmService.shared.realm.write({
            configuration.masterDataExchange = masterDataExchenge
            RealmService.shared.realm.add(configuration)
        })
    }
    
    public func bleSystemSwitch() {
        Helpers.LogCreater.writeLog(string: "BLE LISTENER SWITCHED")
        bleSystem = !bleSystem
        
        let configuration = RealmService.shared.downloadConfigurationFromRLM()
        
        try! RealmService.shared.realm.write({
            configuration.bleListener = bleSystem
            RealmService.shared.realm.add(configuration)
        })
        
        

    }
    
    
    public func generateFakeTransactions() {
        
        for i in 0 ... 2 {
        let characteristicRLM = CharacteristicRlm()
        characteristicRLM.uuid = "EF680101-9B35-4933-9B10-00000000000\(i)"
        characteristicRLM.value = "00000000000\(i)"
        
        let serviceRLM = ServiceRlm()
        serviceRLM.uuid = "EF680100-9B35-4933-9B10-000000000\(i)"
        serviceRLM.characteristics.append(characteristicRLM)
        
        let transactionRLM = TransactionRlm()
        transactionRLM.connectableStatus = "1"
        transactionRLM.fromWhiteList = true
        transactionRLM.peripheralIdentifier = "EF342555-9B35-4933-9B10-000000000\(i)"
        transactionRLM.services.append(serviceRLM)
        
        RealmService.shared.createObjectInRealm(transactionRLM)
        }
        
    }
    
    public func setConfigurations() {
        let configuration = RealmService.shared.downloadConfigurationFromRLM()
        bleSystem = configuration.bleListener
        masterDataExchenge = configuration.masterDataExchange
        transactionsExchange = configuration.transactionalDataExchange
        moecoService = configuration.moecoService
    }
    
    public func createHashOrFetchExisted() {
        switch RealmService.shared.fetchUserHashFromRealm() {
        case "Undefined user":
            RealmService.shared.deleteALL()
            NetworkService.authorization(apiKey: Permanent.Network.token)
        default:
            break
        }
    }
    
    public func configureDataBase() {
        RealmService.shared.createRealm()
    }


}

