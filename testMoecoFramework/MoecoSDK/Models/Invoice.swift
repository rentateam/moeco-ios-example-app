//
//  Invoice.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation

public struct Invoice {
    let status: String
    let sum: String
    let time:  String
    let statusDescribing: String
    
    init(status: String, sum: String, time: String) {
        self.status = status
        self.sum = sum
        self.time = time
        
        switch status {
        case "1":
            statusDescribing = "Init"
        case "2":
            statusDescribing = "Refused"
        case "3":
            statusDescribing = "Valid"
        case "4":
            statusDescribing = "Payment init"
        case "5":
            statusDescribing = "Paid"
        default:
            statusDescribing = "Unknown"
            
        }
    }
}
