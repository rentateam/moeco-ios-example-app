//
//  ConfigurationsRLM.swift
//  moeco
//
//  Created by Алексей Россошанский on 04.04.18.
//  Copyright © 2018 rentateam. All rights reserved.
//

import Foundation
import RealmSwift

//Configuration for settings
class ConfigurationsRLM: Object {
    
    @objc dynamic var id = "1"
    @objc dynamic var transactionalDataExchange = true
    @objc dynamic var masterDataExchange = true
    @objc dynamic var bleListener = true
    @objc dynamic var moecoService = true
    
    override class func primaryKey() -> String {
        return "id"
    }
    
}
